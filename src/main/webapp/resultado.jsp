<%-- 
    Document   : resultado
    Created on : 09-04-2021, 16:59:26
    Author     : Admin
--%>

<%@page import="modelo.Calculador"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<% 
Calculador cal = (Calculador)request.getAttribute("micalculadora"); // rescatamos lo que  el servlet nos esta mandando
%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP RESULTADO</title>
    </head>
    <body>
        <form name="form" action="CalculadoraController" method="POST" >
            <div class="form-group" >
                <label for="codigo"> resultado del calculo </label>
                <h1> <%= cal.InteresSimple()%> </h1>
            </div>                
                
        
        </form>
    </body>
</html>
