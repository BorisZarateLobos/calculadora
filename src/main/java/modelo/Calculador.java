/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Admin
 */
public class Calculador {
    
     /*
    r = interes simple producido
    c = interes anual capital
    i = tasa interes anual
    n = numero de años
    */
    
    private int r;
    private int c;
    private int i;
    private int n;
    

    /**
     * @return the r
     */
    public int getR() {
        return r;
    }

    /**
     * @param r the r to set
     */
    public void setR(int r) {
        this.r = r;
    }

    /**
     * @return the c
     */
    public int getC() {
        return c;
    }

    /**
     * @param c the c to set
     */
    public void setC(int c) {
        this.c = c;
    }

    /**
     * @return the i
     */
    public int getI() {
        return i;
    }

    /**
     * @param i the i to set
     */
    public void setI(int i) {
        this.i = i;
    }

    /**
     * @return the n
     */
    public int getN() {
        return n;
    }

    /**
     * @param n the n to set
     */
    public void setN(int n) {
        this.n = n;
    }
    
    
    public int InteresSimple () {
        return (this.getC() * (this.getI() / 100) * this.getN());
    }
    
    public int InteresSimple (int capital, int interes, int anos) {
        this.setI(interes);
        this.setC(capital);
        this.setN(anos);
        
        return InteresSimple();
    }
    
   // public int InteresSimple (int c, int i, int n) {
   //     this.r = (c*(i/100)*n);
   //     return r;
   // }

   
    
  
}
